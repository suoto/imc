# IMC take home assesment

[TOC]

## IGMP Reply module

The IGMP reply module's architecture is shown in the figure below.

```
              .------------------------------------------------------.
              |                      IGMP reply                      |
              |                                                      |
              |      .---------.  .------------------------------.   |
 RX query  --------->|         |  |  IGMP reply assembler        |   |
 interface    |      |  IGMP   |  |  .---------.   .-----------. |   |
              |      |  cache  |  |  | Reply   |   | Frame     | |   |
     IGMP     |      | lookup  |---->| Request |-->| assembler |--------> Tx reply
     group --------->|         |  |  | Queue   |   '-----------' |   |    interface
     cache    |      |         |  |  '---------'        A        |   |
              |      '---------'  '---------------------|--------'   |
Own MAC/IP ---------------------------------------------'            |
              |                                                      |
              '------------------------------------------------------'
```

* IGMP reply (`ipv4/src/igmp_reply.vhd`)
    * IGMP cache lookup (`ipv4/src/igmp_cache_lookup.vhd`)
        * Stores the configured IGMP IP addresses
        * Receives the IGMP query info and generates reply requests accordingly
    * IGMP reply assembler (`ipv4/src/igmp_reply_assembler.vhd`)
        * Enqueues the reply requests. Because IGMP query is rather slow, this is
          mostly useful when a server sends a general query since it generates a
          reply for every configured IP.
        * Assembles the IGMP reply frames based on a template replacing relevant
          fields with own MAC/IP and IGMP IP address

### Obtaining module and dependencies

To get the IGMP reply module with all dependencies:

    git clone https://bitbucket.org/suoto/imc --recursive

Simulation and unit testing are both based on [VUnit](https://github.com/VUnit/vunit)
with ModelSim.

To install it using `pip`:

    pip install vunit-hdl --user

### Simulating


Tests can be launched from the `ipv4/` folder.

#### Listing tests

```
$ cd ipv4/
$ ./run.py -l
ipv4_tb.igmp_reply_tb.test_all_reply_to_general_query
ipv4_tb.igmp_reply_tb.test_single_reply_to_general_query
ipv4_tb.igmp_reply_tb.test_reply_to_individual_query
ipv4_tb.igmp_reply_tb.test_no_reply_for_individual_query_if_not_configured
ipv4_tb.igmp_reply_tb.test_no_reply_for_general_query_if_not_configured
Listed 5 tests
```

####  Running a single test on GUI

```
./run.py ipv4_tb.igmp_reply_tb.test_all_reply_to_general_query -g
```

After ModelSim launches, launch the test by running the following on the
`Transcript` tab:

```
VSIM 2> do vunit.do
```

####  Running all tests

```
$ ./run.py -l
Starting ipv4_tb.igmp_reply_tb.test_all_reply_to_general_query
pass (P=1 S=0 F=0 T=5) ipv4_tb.igmp_reply_tb.test_all_reply_to_general_query (4.5
seconds)

Starting ipv4_tb.igmp_reply_tb.test_single_reply_to_general_query
pass (P=2 S=0 F=0 T=5) ipv4_tb.igmp_reply_tb.test_single_reply_to_general_query
(2.2 seconds)

Starting ipv4_tb.igmp_reply_tb.test_reply_to_individual_query
pass (P=3 S=0 F=0 T=5) ipv4_tb.igmp_reply_tb.test_reply_to_individual_query (2.2
seconds)

Starting
ipv4_tb.igmp_reply_tb.test_no_reply_for_individual_query_if_not_configured
pass (P=4 S=0 F=0 T=5)
ipv4_tb.igmp_reply_tb.test_no_reply_for_individual_query_if_not_configured (2.2
seconds)

Starting ipv4_tb.igmp_reply_tb.test_no_reply_for_general_query_if_not_configured
pass (P=5 S=0 F=0 T=5)
ipv4_tb.igmp_reply_tb.test_no_reply_for_general_query_if_not_configured (2.2
seconds)

==== Summary ======================================================================================
pass ipv4_tb.igmp_reply_tb.test_all_reply_to_general_query
(4.5 seconds)
pass ipv4_tb.igmp_reply_tb.test_single_reply_to_general_query
(2.2 seconds)
pass ipv4_tb.igmp_reply_tb.test_reply_to_individual_query
(2.2 seconds)
pass ipv4_tb.igmp_reply_tb.test_no_reply_for_individual_query_if_not_configured
(2.2 seconds)
pass ipv4_tb.igmp_reply_tb.test_no_reply_for_general_query_if_not_configured
(2.2 seconds)
===================================================================================================
pass 5 of 5
===================================================================================================
Total time was 13.3 seconds
Elapsed time was 13.6 seconds
===================================================================================================
All passed!
```

---

## FPGA Engineer Assignment

### 1. Which algorithms can you think of to achieve this operation?

* ID hash table
* Dynamic generated code

### 2. Please name the advantages and drawbacks of each one?

#### ID hash table

* Use a hash of the ID instead of the ID itself. Since all IDs are known in
  advance and it is possible to pre-calculate in advance, the hashing
  algorithm can be chosen as such that (1) uses the least amount of bits as
  result and (2) no different IDs result in the same hash
* Advantages:
    * Easy to implement on an FPGA point of view
    * Low latency
* Drawbacks:
    * Hash function must checked every time a change occurs in any ID (of
      interest or not).
    * It may be hard to find a hash algorithm given some set of inputs.

#### Dynamic generated code

* Since all IDs are known in advance, a 2 way mapping between ID and the ID
  map can be implemented by means of conditional testing (`if-then-else` or
  `switch-case`) structure. Assuming the number of IDs of interest is large
  and **not** subject to change *during runtime*, this could be a dynamically
  generated code. Different sets of IDs of interest yield different code and
  thus a different bitstream. In other words, a new
  synthesis/implementation/PAR should be ran.
* Advantages:
    * Probably the best option within the FPGA
* Drawbacks:
    * Need to use a preprocessor tool to
        1. Resolve the associations
        1. Generate the correct RTL code
        1. Insert this into the synthesis tool flow
    * Could be hard to reproduce in simulation/lab issues occurring in field
    * May need tweaking on the constraints to accommodate high fanout and
      register duplication

### 3. How do they perform in terms of throughput and latency?

* ID hash table: same cycle translation, giving no latency and maximum throughput
* Dynamic generated code: same cycle translation, giving no latency and maximum
  throughput

### 4. How would you implement this on an FPGA

#### ID hash table

* Before runtime: find out a suitable hash method for the given IDs. This could
  be written on a high level language.

```
for hash_algorithm in <list of hash algorithms available> do
  # Calculate the hash of all IDs of interest using <hash_algorithm>
  hash_list = [<hash_algorithm>(<ID>) for <ID> in <IDs of interest>]

  # Iterate over the hash list to check each ID/hash pair is unique
  for <hash> in <hash_list> do
    if number of <hash> in <hash_list> != 1 then
      error, we have hash collision
    end if
  end for

  # Now iterate over the hash of all IDs to ensure that no ID which we have no
  # interest on have a hash inside <hash_list>
  for <ID> in <list of all IDs> do
    this_hash = <hash_algorithm>(<ID>)
    if number of <this_hash> in <hash_list> != 0 then
      error, we have hash collision!
    end if
  end for

  if no error was found then
    return <hash_algorithm>
  end if
end for

```

* At runtime: replace the `ID hash calculation` algorithm with the algorithm found previously

```
                           .------------.
         .-------------.   |            |
Input -->|   ID hash   |-->| ID context |--> ID context --> ...
 ID      | calculation |   |   memory   |
         '-------------'   |            |
                           '------------'
```

#### Dynamic generated code

* Before runtime
    1. Define an association between the IDs of interest and their internal ID
    1. Run the code generator with given IDs of interest
    1. Insert the generated code into the synthesis tool flow
    1. Run the synthesis tool and generate the bitstream

* The code generator would generate either a package with a function or a
  translation entity.

### 5. What would be the most suitable one considering the goal is low latency?

The dynamic code option deserves experimenting with first to learn how the tool implements it and what are the timing results.

On the other hand, the hash method is quite similar, requires less effort and provides faster time to market.

When using the ID conversion via function inside a package on both cases, it's easy to switch amongst the alternatives.

### 6. How would you implement your favored algorithm?

The code generator tool could be written either on Tcl to integrate on synthesis
tools more easily or some other higher level language the team prefers.

Using Python-style pseudo-code:

```
def generate_code_translation(id_list, output_file):
    fd = open(output_file, 'w')
    fd.write("vhd header and initial definitions")
    sequence = 0
    for id in id_list:
        fd.write("write if-then-else structure for <id> and <sequence>")
        sequence += 1
    fd.write("closing vhd file sections")
```

The generated code would look similar to the examples below (libraries and means
to make things generic omitted for the sake of clarity)

* As a function (this file should be updated for every change of the IDs of
  interest)

```
function translate_id (
  constant id    : std_logic_vector(31 downto 0)) return std_logic_vector is
  variable value : std_logic_vector(11 downto 0);
  begin
      if id = <some ID of interest> then
        value := conv_std_logic_vector(0, 12);
      elsif id = <another ID of interest> then
        value := conv_std_logic_vector(1, 12);
      ...
      else
        -- If ID was not found, map to all ones so the caller knows it's an
        error!
        value := (others => '1');
      end if;
  end function;
```

* As an entity (this probably don't need to be updated as long the widths match)

```
entity id_translator is
    generic map (
        VALUE_WIDTH : 12);
    port map (
        id    : in  std_logic_vector(31 downto 0);
        value : out std_logic_vector(VALUE_WIDTH - 1 downto 0);
        error : out std_logic);
end entity id_translator;

architecture rtl of id_translator is

    signal value_i : std_logic_vector(VALUE_WIDTH - 1 downto 0);

begin

  value_i <= translate_id(id);
  value   <= value_i;
  -- Flag match errors
  error   <= '1' when value_i = (VALUE_WIDTH - 1 downto 0 => '1') else '0';

end rtl;
```

