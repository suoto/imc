add  wave  -noupdate  -expand  -group  "TB top" "*"
add  wave  -noupdate -group  "DUT"    "dut/*"
add  wave  -noupdate -expand -group  "IGMP cache lookup" \
    "dut/igmp_cache_lookup_u/*"
add  wave  -noupdate -group  "IGMP cache lookup RAM" \
    "dut/igmp_cache_lookup_u/igmp_groups_u/*"
add  wave  -noupdate  -group  "IGMP reply assembler" \
    "dut/igmp_reply_assembler_u/*"

add  wave  -noupdate  -group  "IGMP reply assembler FIFO" \
    "dut/igmp_reply_assembler_u/request_queue_u/*"
#
# # Some signals are better viewed on different radix
# add wave -noupdate -radix bin -expand -group "DUT"    "dut/encoded_data"
# add wave -noupdate -radix uns -expand -group "DUT"    "dut/output_bit_cnt"
# add wave -noupdate -radix uns -expand -group "DUT"    "dut/encoded_dwidth"
configure wave -namecolwidth 200
configure wave -valuecolwidth 120
update
