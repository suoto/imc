#!/usr/bin/env python
'IGMP reply unit test runner'

from os.path import join, dirname
from vunit.verilog import VUnit

def main():
    ui = VUnit.from_argv()
    ui.add_osvvm()
    #  ui.add_com()
    ui.disable_ieee_warnings()

    src_path = join(dirname(__file__), 'src')

    ipv4 = ui.add_library('ipv4')
    ipv4.add_source_files(join(src_path, '*.vhd'))
    ipv4.add_source_files(join(src_path, '*.v'))

    ipv4_tb = ui.add_library('ipv4_tb')
    #  ipv4_tb.add_source_files(join(src_path, 'test', '*.vhd'))
    ipv4_tb.add_source_files(join(src_path, 'test', '*.sv'))

    str_format = ui.add_library('str_format')
    str_format.add_source_file(
        '../dependencies/hdl_string_format/src/str_format_pkg.vhd')

    for library_name in ('common_lib', 'memory'):
        library = ui.add_library(library_name)
        library.add_source_files(
            '../dependencies/hdl_lib/' + library_name + '/*.vhd')

    ui.set_compile_option('modelsim.vcom_flags', ['-novopt', '-explicit'])
    ui.set_sim_option('modelsim.vsim_flags', ['-novopt'])
    ui.main()

if __name__ == '__main__':
    import sys
    sys.exit(main())

