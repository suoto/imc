---------------------------------
-- Block name and description --
--------------------------------

---------------
-- Libraries --
---------------
library	ieee;
    use ieee.std_logic_1164.all;  
    use ieee.std_logic_arith.all;			   

library common_lib;
library ipv4;

------------------------
-- Entity declaration --
------------------------
entity igmp_reply is
    port (
        -- Clocks and resets
        rx_clk               : in  std_logic;
        rx_res_n             : in  std_logic;
        tx_clk               : in  std_logic;
        tx_res_n             : in  std_logic;
        -- Rx interface
        rx_igmp_shift_out    : out std_logic;
        rx_igmp_multicast_ip : in  std_logic_vector(31 downto 0);
        rx_igmp_fifo_empty   : in  std_logic;
        -- Tx interface
        tx_fifo_afull        : in  std_logic;
        tx_data              : out std_logic_vector(63 downto 0);
        tx_shift_in          : out std_logic;
        tx_sof               : out std_logic;
        tx_eof               : out std_logic;
        -- Ethernet frame info
        own_ip_address       : in  std_logic_vector(31 downto 0);
        own_mac_address      : in  std_logic_vector(47 downto 0);
        -- IGMP group memory
        igmp_address         : in  std_logic_vector(4 downto 0);
        igmp_write_en        : in  std_logic;
        igmp_write_data      : in  std_logic_vector(31 downto 0));
end igmp_reply;

architecture igmp_reply of igmp_reply is

    -----------
    -- Types --
    -----------

    -------------
    -- Signals --
    -------------
    -- Active high resets
    signal rx_rst_p             : std_logic;
    signal tx_rst_p             : std_logic;
    -- 
    signal tx_igmp_fifo_empty   : std_logic;
    signal tx_igmp_multicast_ip : std_logic_vector(31 downto 0);
    -- IGMP query request interface
    signal igmp_lookup_en       : std_logic;
    signal igmp_target_ip       : std_logic_vector(31 downto 0);
    signal igmp_lookup_busy     : std_logic;
    -- Interface with the reply frame assembler
    signal query_reply_en       : std_logic;
    signal query_reply_ip       : std_logic_vector(31 downto 0);

begin

    -------------------
    -- Port mappings --
    -------------------
    -- The rx_igmp_multicast_ip signal is valid when rx_igmp_fifo_empty is 0
    -- and it should only change when we use rx_igmp_shift_out. This means we can
    -- simply sample rx_igmp_multicast_ip input on tx_clk to avoid metastability.
    -- A from-to constraint should be added to the UCF
    multicast_ip_sample_u : entity common_lib.sr_delay
        generic map (
            DELAY_CYCLES => 3,
            DATA_WIDTH   => 33)
        port map (
            clk               => tx_clk,
            clken             => '1',

            din(32)           => rx_igmp_fifo_empty,
            din(31 downto 0)  => rx_igmp_multicast_ip,
            dout(32)          => tx_igmp_fifo_empty,
            dout(31 downto 0) => tx_igmp_multicast_ip);

    igmp_cache_lookup_u : entity ipv4.igmp_cache_lookup
        generic map (
            CACHE_ADDR_WIDTH => 5)
        port map (
            -- Usual ports
            clk             => tx_clk,
            rst             => tx_rst_p,
            -- IGMP query request interface
            query_en       => igmp_lookup_en,
            query_ip       => igmp_target_ip,
            query_busy     => igmp_lookup_busy,
            -- Interface with the reply frame assembler
            reply_en        => query_reply_en,
            reply_ip        => query_reply_ip,
            -- IGMP group memory
            igmp_address    => igmp_address,
            igmp_write_en   => igmp_write_en,
            igmp_write_data => igmp_write_data);

    igmp_reply_assembler_u : entity ipv4.igmp_reply_assembler
        port map (
            -- Usual ports
            clk             => tx_clk,
            rst             => tx_rst_p,
            -- Ethernet frame info
            own_ip_address  => own_ip_address,
            own_mac_address => own_mac_address,
            -- Reply request queue
            reply_en        => query_reply_en,
            reply_ip        => query_reply_ip,
            -- Tx interface
            tx_fifo_afull   => tx_fifo_afull,
            tx_data         => tx_data,
            tx_shift_in     => tx_shift_in,
            tx_sof          => tx_sof,
            tx_eof          => tx_eof);

    -- Use the IGMP lookup query_busy indication to flag to the IGMP request FIFO that
    -- it can remove a query from its FIFO
    igmp_lookup_busy_rising_edge_u : entity common_lib.edge_detector
        generic map (
            SYNCHRONIZE_INPUT => True,
            OUTPUT_DELAY      => 1)
        port map (
            -- Usual ports
            clk     => rx_clk,
            clken   => '1',

            -- 
            din     => igmp_lookup_busy,
            -- Edges detected
            rising  => rx_igmp_shift_out,
            falling => open,
            toggle  => open);

    -----------------------------
    -- Asynchronous assignments --
    -----------------------------
    rx_rst_p <= not rx_res_n;
    tx_rst_p <= not tx_res_n;

    ---------------
    -- Processes --
    ---------------
    process(tx_clk, tx_res_n)
    begin
        -- Active low asynchronous reset
        if tx_res_n = '0' then
        elsif tx_clk'event and tx_clk = '1' then

            igmp_lookup_en <= '0';

            -- If we get a general query, insert a reply request for each configured
            -- address
            if igmp_lookup_busy = '0' and tx_igmp_fifo_empty = '0' then
                igmp_lookup_en <= '1';
                igmp_target_ip <= tx_igmp_multicast_ip;
            end if;
        end if;
    end process;

end igmp_reply;

