---------------------------------
-- Block name and description --
--------------------------------

---------------
-- Libraries --
---------------
library	ieee;
    use ieee.std_logic_1164.all;
    use ieee.std_logic_arith.all;
    use ieee.std_logic_unsigned.all;

library common_lib;
    use common_lib.common_pkg.all;

library ipv4;
    use ipv4.ether_frames_pkg.all;
library memory;

------------------------
-- Entity declaration --
------------------------
entity igmp_reply_assembler is
    generic (
        REPLY_QUEUE_LENGTH : integer := 32);
    port (
        -- Usual ports
        clk             : in  std_logic;
        rst             : in  std_logic;
        -- Ethernet frame info
        own_ip_address  : in  std_logic_vector(31 downto 0);
        own_mac_address : in  std_logic_vector(47 downto 0);
        -- Reply request queue
        reply_en        : in std_logic;
        reply_ip        : in std_logic_vector(31 downto 0);
        -- Tx interface
        tx_fifo_afull   : in  std_logic;
        tx_data         : out std_logic_vector(63 downto 0);
        tx_shift_in     : out std_logic;
        tx_sof          : out std_logic;
        tx_eof          : out std_logic);
end igmp_reply_assembler;

architecture igmp_reply_assembler of igmp_reply_assembler is

    -----------
    -- Types --
    -----------
    type ctrl_fsm_t is (idle_st, write_st);
    ---------------
    -- Constants --
    ---------------
    constant REPLY : igmp_frame_t := (
        dst_mac            => MAC_IPv4_MCAST,   --
        src_mac            => (others => '0'),  -- Filled later
        ether_type         => ETHER_TYPE_IPv4,  -- 0x0800
        ip_version         => x"4",             --
        ip_ihl             => x"5",             --
        ip_total_length    => x"0020",          --
        ip_proto           => x"02",            --
        ip_checksum        => (others => '0'),  --
        ip_src             => (others => '0'),  -- Filled later
        ip_dst             => (others => '0'),  -- Filled later
        igmp_type          => IGMP_REPORT_v2,
        igmp_max_resp_time => x"64",
        igmp_checksum      => (others => '0'),  --
        igmp_address       => (others => '0')); -- Filled later

    constant BASE_FRAME : vec64_array_t :=
        (0 => REPLY.dst_mac              & REPLY.src_mac(47 downto 32),
         1 => REPLY.src_mac(31 downto 0) & REPLY.ether_type             & REPLY.ip_version    & REPLY.ip_ihl         & x"00",
         2 => x"0000"                    & x"0000"                      & x"01"               & REPLY.ip_proto       & REPLY.ip_checksum, -- Identification / fragment offset / TTL
         3 => REPLY.ip_src               & REPLY.ip_dst,
         4 => REPLY.igmp_type            & REPLY.igmp_max_resp_time     & REPLY.igmp_checksum & REPLY.igmp_address);

    constant BASE_FRAME_WIDTH : integer := BASE_FRAME'length;

    -------------
    -- Signals --
    -------------
    signal rq_framer_ip    : std_logic_vector(31 downto 0);
    signal current_ip      : std_logic_vector(31 downto 0);
    signal rq_rden         : std_logic;
    signal reply_fifo_full : std_logic;
    -- Request queue read side
    signal rq_empty : std_logic;

    signal tx_dv           : std_logic;
    signal tx_sof_i        : std_logic;
    signal tx_eof_i        : std_logic;

    signal ctrl_fsm        : ctrl_fsm_t := idle_st;
    signal word_cnt        : std_logic_vector(numbits(BASE_FRAME_WIDTH) - 1 downto 0);

begin

    -------------------
    -- Port mappings --
    -------------------
    request_queue_u : entity memory.async_fifo
        generic map (
            -- FIFO configuration
            FIFO_LEN         => REPLY_QUEUE_LENGTH,
            DATA_WIDTH       => 32,
            -- FIFO config for error cases
            OVERFLOW_ACTION  => "SATURATE",
            UNDERFLOW_ACTION => "SATURATE"
        )
        port map (
            -- Write port
            wr_clk      => clk,
            wr_clken    => '1',
            wr_arst     => rst,
            wr_data     => reply_ip,
            wr_en       => reply_en,
            wr_full     => reply_fifo_full,
            wr_upper    => open,

            -- Read port
            rd_clk      => clk,
            rd_clken    => '1',
            rd_arst     => rst,
            rd_data     => rq_framer_ip,
            rd_en       => rq_rden,
            rd_dv       => open,
            rd_lower    => open,
            rd_empty    => rq_empty);

    -----------------------------
    -- Asynchronous assignments --
    -----------------------------
    tx_shift_in  <= tx_dv;
    tx_sof       <= tx_sof_i when tx_dv = '1' else '0';
    tx_eof       <= tx_eof_i when tx_dv = '1' else '0';

    ---------------
    -- Processes --
    ---------------
    frame_assembly : process(clk, rst)
    begin
        if rst = '1' then
            tx_dv <= '0';
            word_cnt    <= (others => '0');
            ctrl_fsm    <= idle_st;
        elsif clk'event and clk = '1' then
            rq_rden <= '0';

            -- IGMP is rather slow, so no need to chain multiple frames. It's better to
            -- just make things maintainable and readable
            case ctrl_fsm is
                when idle_st =>
                    tx_dv        <= '0';
                    -- If we have pending requests to assemble and the transmission FIFO
                    -- has room for another frame, start assembling it
                    if rq_empty = '0' and tx_fifo_afull = '0' then
                        word_cnt   <= (others => '0');
                        rq_rden    <= '1';
                        current_ip <= rq_framer_ip;
                        ctrl_fsm   <= write_st;
                    end if;
                when write_st =>
                    if word_cnt < BASE_FRAME_WIDTH - 1 then
                        tx_dv <= '1';
                        word_cnt    <= word_cnt + 1;
                    else
                        ctrl_fsm <= idle_st;
                    end if;
                when others =>
                    ctrl_fsm <= idle_st;
            end case;

            -- Controls for the start and end of frame flags
            tx_sof_i     <= '0';
            tx_eof_i     <= '0';

            if word_cnt = 0 then
                tx_sof_i <= '1';
            end if;
            if word_cnt = BASE_FRAME_WIDTH - 1 then
                tx_eof_i <= '1';
            end if;

            tx_data <= BASE_FRAME(conv_integer(word_cnt));

            case conv_integer(word_cnt) is
                when 0 =>
                    tx_data(15 downto 0)  <= own_mac_address(47 downto 32);
                when 1 =>
                    tx_data(63 downto 32) <= own_mac_address(31 downto 0);
                when 3 =>
                    tx_data               <= own_ip_address & current_ip;
                when 4 =>
                    tx_data(31 downto 0)  <= current_ip;
                when others =>
                    null;
            end case;

        end if;
    end process;


end igmp_reply_assembler;


