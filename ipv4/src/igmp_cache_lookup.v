
module igmp_cache_lookup
    #(parameter CACHE_ADDR_WIDTH = 5)
    (// Usual ports
    input clk,
    input rst,
    // IGMP query request interface
    input        query_en,
    input [31:0] query_ip,
    output       query_busy,
    // Interface with the reply frame assembler
    output        reply_en,
    output [31:0] reply_ip,
    // IGMP group memory
    input [CACHE_ADDR_WIDTH - 1:0] igmp_address,
    input                          igmp_write_en,
    input [31:0]                   igmp_write_data);

/*************
 * Registers *
 *************/
reg [CACHE_ADDR_WIDTH - 1:0] rd_addr;
reg [31:0]                   cached_ip;
reg done;
reg busy;
reg rd_dv;
reg reply_en_i;
reg [31:0] reply_ip_i;

/****************************
 * Asynchronous assignments *
 ****************************/
assign query_busy = busy;
assign reply_en = reply_en_i;
assign reply_ip = reply_ip_i;

/*****************
 * Port mappings *
 *****************/
\memory.ram_inference #(
    .ADDR_WIDTH(CACHE_ADDR_WIDTH),
    .DATA_WIDTH(32),
    .EXTRA_OUTPUT_DELAY(1))
igmp_groups_u (
    // Port A
    .clk_a     (clk),
    .clken_a   (1'b1),
    .wren_a    (igmp_write_en),
    .addr_a    (igmp_address),
    .wrdata_a  (igmp_write_data),
    .rddata_a  (),

    // Port B
    .clk_b     (clk),
    .clken_b   (1'b1),
    .addr_b    (rd_addr),
    .rddata_b  (cached_ip));

\common_lib.sr_delay #(
    .DELAY_CYCLES(2),
    .DATA_WIDTH(1))
busy_delay_u (
    .clk   (clk),
    .clken (1'b1),
    .din   (busy),
    .dout  (rd_dv));

always @ (posedge clk or posedge rst)
    if (rst == 1'b1) begin
        rd_addr <= 2**CACHE_ADDR_WIDTH - 1;
        busy    <= 0;
    end
    else begin
        if (query_en == 1 && busy == 0) begin
            $display("Will start reading addresses");
            rd_addr <= 0;
            busy    <= 1;
        end
        else begin
            if (rd_addr < 2**CACHE_ADDR_WIDTH - 1) begin
                $display("Reading address 0x%x", rd_addr);
                busy    <= 1;
                rd_addr <= rd_addr + 1;
            end
            else begin
                busy    <= 0;
            end
        end
    end

always @ (posedge clk)
    begin
        done       <= 0;
        reply_en_i <= 0;
        if (rd_dv == 1) begin
            reply_ip_i <= cached_ip;
            if (cached_ip != 0 &&
                (query_ip == cached_ip || query_ip == {8'd224, 16'b0, 8'b1})) begin
                reply_en_i <= 1;
                $display("Replying to IP %x, cached IP is %x", reply_ip_i, cached_ip);
            end
        end
    end

endmodule

