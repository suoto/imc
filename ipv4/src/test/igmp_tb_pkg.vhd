---------------
-- Libraries --
---------------
use std.textio.all;
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.std_logic_arith.all;
    use ieee.std_logic_unsigned.all;

library ipv4;
    use ipv4.ether_frames_pkg.all;

library str_format;
    use str_format.str_format_pkg.all;

package igmp_tb_pkg is

    ---------------
    -- Constants --
    ---------------

    -----------
    -- Types --
    -----------

    ---------------
    -- Functions --
    ---------------
    -- Formats a vec8_array_t for printing
    impure function format (
        constant frame : vec8_array_t;
        constant break : integer := 16) return string;

    ----------------
    -- Generators --
    ----------------
    procedure clk_gen (
        signal   clk    : out std_logic;
        constant period : in  time;
        constant offset : in  time := 0 ns);

    procedure walk (
        signal   clk   : in std_logic;
        constant steps : natural := 1);

end package;


package body igmp_tb_pkg is

    ---------------
    -- Functions --
    ---------------
    -- Formats a vec8_array_t for printing
    impure function format (
        constant frame : vec8_array_t;
        constant break : integer := 16) return string is
        variable L     : line;
    begin
        -- Print a header first
        write(L, string'("     "));
        for i in 0 to break - 1 loop
            write(L, sformat(" %-4d ", fo(i)));
        end loop;

        write(L, cr & sformat("%3d", fo(0)));

        for i in 0 to frame'length - 1 loop
            write(L, sformat(" %r ", fo(frame(i))));
            if ((i + 1) mod break) = 0 then
                write(L, cr & sformat("%3d", fo(i + 1)));
            end if;
        end loop;

        return L.all;

    end function;



    --
    procedure clk_gen (
        signal   clk    : out std_logic;
        constant period : in  time;
        constant offset : in  time := 0 ns) is
    begin
        wait for offset;
        while True loop
            clk <= '0';
            wait for period/2;
            clk <= '1';
            wait for period/2;
        end loop;
    end procedure;

    --
    procedure walk (
        signal   clk   : in std_logic;
        constant steps : natural := 1) is
    begin
        if steps /= 0 then
            for step in 0 to steps - 1 loop
                wait until rising_edge(clk);
            end loop;
        end if;
    end procedure;


end package body;

