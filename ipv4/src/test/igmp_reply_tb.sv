// You do not need to worry about adding vunit_defines.svh to your
// include path, VUnit will automatically do that for you if VUnit is
// correctly installed (and your python run-script is correct).
`include "vunit_defines.svh"

module igmp_reply_tb;

  localparam integer CLK_PERIOD = 20; // ns

  logic rx_clk = 1'b0;
  logic rx_res_n = 1'b0;

  logic tx_clk = 1'b0;
  logic tx_res_n = 1'b0;

  // Rx interface
  logic        rx_igmp_shift_out;
  logic [31:0] rx_igmp_multicast_ip = 0;
  logic        rx_igmp_fifo_empty = 1'b1;
  // Tx interface
  logic        tx_fifo_afull = 1'b0;
  logic [63:0] tx_data;
  logic        tx_shift_in;
  logic        tx_sof;
  logic        tx_eof;
  // Ethernet frame info
  logic [31:0] own_ip_address;
  logic [47:0] own_mac_address;
  // IGMP group memory
  logic [4:0]  igmp_address = 0;
  logic        igmp_write_en = 1'b0;
  logic [31:0] igmp_write_data = 0;

  //
  // task automatic insert_query (integer ip);
  //   begin
  //     $display("[%t] Inserting request with IP %d", $time, ip);
  //     rx_igmp_multicast_ip <= ip;
  //     rx_igmp_fifo_empty   <= 0;
  //     @(posedge rx_igmp_shift_out);
  //     @(posedge rx_clk);
  //     rx_igmp_fifo_empty   <= 1;
  //   end
  // endtask;

  class query_queue_t;
    integer size;
    integer wr_ptr;
    integer rd_ptr;
    logic [31:0] query_queue[];

    logic        rx_igmp_shift_out;
    logic [31:0] rx_igmp_multicast_ip = 0;
    logic        rx_igmp_fifo_empty = 1'b1;

    function new (integer size);
      this.size = size;
      this.wr_ptr = 0;
      this.rd_ptr = 0;
      this.query_queue = new[this.size];
      rx_igmp_fifo_empty = 1;
    endfunction

    function logic empty;
      if (this.wr_ptr == this.rd_ptr) begin
        return 1'b1;
      end else begin
        return 1'b0;
      end
    endfunction

    task insert_query (integer ip);
      begin
        this.query_queue[wr_ptr] = ip;
        this.wr_ptr++;
        rx_igmp_fifo_empty = 0;
      end
    endtask

  endclass

  query_queue_t query_queue = new(16);

  // The watchdog macro is optional, but recommended. If present, it
  // must not be placed inside any initial or always-block.
  `WATCHDOG(1ms);

  // Clock generation
  always begin
    #(CLK_PERIOD/2 * 1ns);
    rx_clk <= ~rx_clk;
    tx_clk <= ~tx_clk;
  end

  // always @ (posedge rx_clk) begin
  //   rx_igmp_fifo_empty <= query_queue.empty;
  // end

  // Connect the DUT
  \ipv4.igmp_reply dut (.*);


  `TEST_SUITE begin

    `TEST_SUITE_SETUP begin
      $display("[%t] Running test suite setup code", $time);
      repeat (5) @ (posedge rx_clk);
      rx_res_n = ~rx_res_n;
      tx_res_n = ~tx_res_n;
      repeat (5) @ (posedge rx_clk);
    end

    `TEST_CASE_SETUP begin
      $display("Running test case setup code");
    end

    `TEST_CASE("Some test") begin
      $display("Running some test");
      query_queue.insert_query(32'h1234);
      query_queue.insert_query(32'h1235);
      // insert_query(32'h1234);
      // insert_query(32'h4321);
      repeat (10) @ (posedge rx_clk);
    end

    `TEST_CASE_CLEANUP begin
      // This section will run after the end of a test case. In
      // many cases this section will not be needed.
      $display("Cleaning up after a test case");
      `CHECK_EQUAL(query_queue.empty, 0);
    end

    `TEST_SUITE_CLEANUP begin
      // This section will run last before the TEST_SUITE block
      // exits. In many cases this section will not be needed.
      $display("Cleaning up after running the complete test suite");
      `CHECK_EQUAL(query_queue.empty, 0);
    end
  end;
        /*
         * -- Clocks and resets
         * rx_clk               : in  std_logic;
         * rx_res_n             : in  std_logic;
         * tx_clk               : in  std_logic;
         * tx_res_n             : in  std_logic;
         * -- Rx interface
         * rx_igmp_shift_out    : out std_logic;
         * rx_igmp_multicast_ip : in  std_logic_vector(31 downto 0);
         * rx_igmp_fifo_empty   : in  std_logic;
         * -- Tx interface
         * tx_fifo_afull        : in  std_logic;
         * tx_data              : out std_logic_vector(63 downto 0);
         * tx_shift_in          : out std_logic;
         * tx_sof               : out std_logic;
         * tx_eof               : out std_logic;
         * -- Ethernet frame info
         * own_ip_address       : in  std_logic_vector(31 downto 0);
         * own_mac_address      : in  std_logic_vector(47 downto 0);
         * -- IGMP group memory
         * igmp_address         : in  std_logic_vector(4 downto 0);
         * igmp_write_en        : in  std_logic;
         * igmp_write_data      : in  std_logic_vector(31 downto 0));
         */

endmodule

