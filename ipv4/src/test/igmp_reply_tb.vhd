---------------------------------
-- Block name and description --
--------------------------------

---------------
-- Libraries --
---------------
use std.textio.all;
library	ieee;
    use ieee.std_logic_1164.all;  
    use ieee.std_logic_arith.all;			   

library str_format;
    use str_format.str_format_pkg.all;

library vunit_lib;
    context vunit_lib.vunit_context;
    context vunit_lib.com_context;

library osvvm;
    use osvvm.RandomPkg.all;

library ipv4;
    use ipv4.ether_frames_pkg.all;

library ipv4_tb;
    use ipv4_tb.igmp_tb_pkg.all;

------------------------
-- Entity declaration --
------------------------
entity igmp_reply_tb is
    generic (
        RUNNER_CFG             : string  := "";
        TX_FIFO_FULL_RANDOMIZE : integer := 2);
end igmp_reply_tb;

architecture igmp_reply_tb of igmp_reply_tb is

    ---------------
    -- Constants --
    ---------------
    constant RX_CLK_PERIOD : time := 6.4 ns;
    constant TX_CLK_PERIOD : time := 6.4 ns;

    constant OUR_MAC : std_logic_vector(47 downto 0) := x"01_12_23_34_45_56";
    constant OUR_IP  : std_logic_vector(31 downto 0) := dot_decimal_to_hex(10, 20, 30, 40);

    ----------------------
    -- Shared variables --
    ----------------------
    shared variable random_gen  : RandomPType;
    -------------
    -- Signals --
    -------------
    signal rx_clk               : std_logic;
    signal rx_res_n             : std_logic;
    signal rx_igmp_shift_out    : std_logic;
    signal rx_igmp_multicast_ip : std_logic_vector(31 downto 0);
    signal rx_igmp_fifo_empty   : std_logic;

    signal tx_clk               : std_logic;
    signal tx_res_n             : std_logic;
    signal tx_fifo_afull        : std_logic;
    signal tx_data              : std_logic_vector(63 downto 0);
    signal tx_shift_in          : std_logic;
    signal tx_sof               : std_logic;
    signal tx_eof               : std_logic;

    signal igmp_address         : std_logic_vector(4 downto 0);
    signal igmp_write_en        : std_logic;
    signal igmp_write_data      : std_logic_vector(31 downto 0);

begin

    -------------------
    -- Port mappings --
    -------------------
    dut : entity ipv4.igmp_reply
        port map (
            -- Clocks and resets
            rx_clk               => rx_clk,
            rx_res_n             => rx_res_n,
            tx_clk               => tx_clk,
            tx_res_n             => tx_res_n,
            -- Rx interface
            rx_igmp_shift_out    => rx_igmp_shift_out,
            rx_igmp_multicast_ip => rx_igmp_multicast_ip,
            rx_igmp_fifo_empty   => rx_igmp_fifo_empty,
            -- Tx interface
            tx_fifo_afull        => tx_fifo_afull,
            tx_data              => tx_data,
            tx_shift_in          => tx_shift_in,
            tx_sof               => tx_sof,
            tx_eof               => tx_eof,
            -- Ethernet frame info
            own_ip_address       => OUR_IP,
            own_mac_address      => OUR_MAC,
            -- IGMP group memory
            igmp_address         => igmp_address,
            igmp_write_en        => igmp_write_en,
            igmp_write_data      => igmp_write_data);

    -----------------------------
    -- Asynchronous assignments --
    -----------------------------
    test_runner_watchdog(runner, 5 us);

    clk_gen(rx_clk, RX_CLK_PERIOD);
    clk_gen(tx_clk, TX_CLK_PERIOD, TX_CLK_PERIOD/4);

    rx_res_n <= '0', '1' after 10*RX_CLK_PERIOD;
    tx_res_n <= '0', '1' after 10*TX_CLK_PERIOD;

    ---------------
    -- Processes --
    ---------------
    main : process
        variable L       : line;
        variable self    : actor_t := create("checker");
        variable message : message_ptr_t;
        variable status  : com_status_t;
        variable receipt : receipt_t;

        -- Writes an IP to the IGMP cache
        procedure write_group_cache (
            constant address : integer;
            constant ip      : std_logic_vector(31 downto 0)) is
        begin
            igmp_address    <= conv_std_logic_vector(address, 5);
            igmp_write_data <= ip;
            igmp_write_en   <= '1';
            wait until tx_clk = '1';
            igmp_address    <= (others => 'U');
            igmp_write_data <= (others => 'U');
            igmp_write_en   <= '0';
        end procedure;

        -- Writes all zeros into all IGMP cache channels
        procedure clear_igmp_cache is
        begin
            wait until tx_clk = '1';
            for addr in 0 to 31 loop
                write_group_cache(addr, (others => '0'));
            end loop;
        end procedure;

        -- Receives a reply through the Tx interface
        procedure get_reply (
                     frame    : out igmp_frame_t) is
            variable word_cnt : integer;
            variable L        : line;
        begin
            wait until tx_sof = '1';
            word_cnt := 0;

            while True loop
                walk(tx_clk, 1);
                if tx_shift_in = '1' then
                    case word_cnt is
                        when 0 =>
                            frame.dst_mac               := tx_data(63 downto 16);
                            frame.src_mac(47 downto 32) := tx_data(15 downto 0);
                        when 1 => 
                            frame.src_mac(31 downto 0) := tx_data(63 downto 32);
                            frame.ether_type           := tx_data(31 downto 16);
                            frame.ip_version           := tx_data(15 downto 12);
                        when 2 =>
                            frame.ip_proto    := tx_data(23 downto 16);
                            frame.ip_checksum := tx_data(15 downto 0);
                        when 3 =>
                            frame.ip_src := tx_data(63 downto 32);
                            frame.ip_dst := tx_data(31 downto 0);
                        when 4 =>
                            frame.igmp_type          := tx_data(63 downto 56);
                            frame.igmp_max_resp_time := tx_data(55 downto 48);
                            frame.igmp_checksum      := tx_data(47 downto 32);
                            frame.igmp_address       := tx_data(31 downto 0);
                        when others =>
                            assert False;
                    end case;

                    word_cnt := word_cnt + 1;

                end if;
                exit when tx_eof = '1';
            end loop;
        end procedure;

        -- Makes a pretty string representation of a frame
        impure function format(
            constant frame : igmp_frame_t) return string is
            variable L     : line;
        begin
            write(L, string'("    "));
            write(L, sformat("MAC dest: %r || ",   fo(frame.dst_mac)));
            write(L, sformat("MAC src:  %r || ",      fo(frame.src_mac)));
            write(L, sformat("Ether type: %r" & cr,  fo(frame.ether_type)));

            write(L, string'("    "));
            write(L, sformat("IP version: %r          || ",      fo(frame.ip_version)));
            write(L, sformat("IP ihl: %r              || ",            fo(frame.ip_ihl)));
            write(L, sformat("IP total_length: %r || ",   fo(frame.ip_total_length)));
            write(L, sformat("IP proto: %r || ",          fo(frame.ip_proto)));
            write(L, sformat("IP checksum: %r || " & cr,  fo(frame.ip_checksum)));

            write(L, string'("    "));
            write(L, sformat("IP src: %r       || ",          fo(frame.ip_src)));
            write(L, sformat("IP dst: %r" & cr,          fo(frame.ip_dst)));

            write(L, string'("    "));
            write(L, sformat("IGMP type: %r          || ", fo(frame.igmp_type)));
            write(L, sformat("IGMP resp time: %r     || ", fo(frame.igmp_max_resp_time)));
            write(L, sformat("IGMP checksum: %r || ", fo(frame.igmp_checksum)));
            write(L, sformat("IGMP address:  %r", fo(frame.igmp_address)));

            return L.all;

        end function;

        -- Checks that a query is generated for every IP that has been
        -- configured
        procedure check_general_query_reply is
            variable frame    : igmp_frame_t;
        begin
            for i in 0 to 31 loop
                get_reply(frame);
                info("Frame received: " & cr & format(frame));

                check_equal(frame.ether_type, ETHER_TYPE_IPv4,
                            "Received wrong ether type");
                check_equal(frame.ip_version, std_logic_vector'(x"4"),
                            "Received Wrong IP version");
                check_equal(frame.ip_src, OUR_IP,
                            "Received wrong source IP address");
                check_equal(frame.src_mac, OUR_MAC,
                            "Received wrong source MAC source");
                check_equal(frame.dst_mac, MAC_IPv4_MCAST,
                            "Received wrong dest MAC source");
                check_equal(frame.igmp_address, dot_decimal_to_hex(i, i + 1, i + 2, i + 3),
                            "Received wrong IP address");
            end loop;
        end procedure;

        -- Checks that a query is generated for every IP that has been
        -- configured
        procedure check_single_reply_to_general_query (
            constant ip    : std_logic_vector(31 downto 0)) is
            variable frame : igmp_frame_t;
        begin
            get_reply(frame);
            info("Frame received: " & cr & format(frame));

            check_equal(frame.ether_type, ETHER_TYPE_IPv4,
                        "Received wrong ether type");
            check_equal(frame.ip_version, std_logic_vector'(x"4"),
                        "Received Wrong IP version");
            check_equal(frame.ip_src, OUR_IP,
                        "Received wrong source IP address");
            check_equal(frame.src_mac, OUR_MAC,
                        "Received wrong source MAC source");
            check_equal(frame.dst_mac, MAC_IPv4_MCAST,
                        "Received wrong dest MAC source");
            check_equal(frame.igmp_address, ip,
                        "Received wrong IP address");
        end procedure;

        variable stat   : checker_stat_t;
        variable filter : log_filter_t;

    begin
        checker_init(display_format => verbose,
                     file_name      => join(output_path(RUNNER_CFG), "error.csv"),
                     file_format    => verbose_csv);
        logger_init(display_format => verbose,
                    file_name      => join(output_path(RUNNER_CFG), "log.csv"),
                    file_format    => verbose_csv);
        stop_level(verbose, display_handler, filter);
        test_runner_setup(runner, RUNNER_CFG);

        wait until rx_res_n = '1';
        walk(rx_clk, 16);
        clear_igmp_cache;
        walk(rx_clk, 16);

        while test_suite loop
            if run("test_all_reply_to_general_query") then
                -- Write a valid IP address into all cache positions
                walk(tx_clk);
                for i in 0 to 31 loop
                    write_group_cache(i, dot_decimal_to_hex(i, i + 1, i + 2, i + 3));
                end loop;
                walk(rx_clk);

                -- Tell the query generator to insert a general query
                send(net, find("generator"), "insert_general_query", receipt);
                check(receipt.status = ok);

                check_general_query_reply;
            elsif run("test_single_reply_to_general_query") then
                -- Only a single channel is properly configured
                walk(tx_clk);
                write_group_cache(10, dot_decimal_to_hex(192, 168, 0, 1));
                walk(rx_clk);

                -- Tell the query generator to insert a general query
                send(net, find("generator"), "insert_general_query", receipt);
                check(receipt.status = ok);

                check_single_reply_to_general_query(dot_decimal_to_hex(192, 168, 0, 1));
            elsif run("test_reply_to_individual_query") then
                -- Only a single channel is properly configured
                walk(tx_clk);
                write_group_cache(20, dot_decimal_to_hex(10, 10, 10, 10));
                walk(rx_clk);

                -- Tell the query generator to insert an individual query to 10.10.10.10
                send(net, find("generator"), "insert_individual_query", receipt);
                check(receipt.status = ok);

                check_single_reply_to_general_query(dot_decimal_to_hex(10, 10, 10, 10));
            elsif run("test_no_reply_for_individual_query_if_not_configured") then
                -- Configure a single channel but with an IP address that doesn't matches
                -- the individual query we will send
                walk(tx_clk);
                write_group_cache(20, dot_decimal_to_hex(10, 20, 30, 40));
                walk(rx_clk);
                -- Tell the query generator to insert an individual query to 10.10.10.10
                send(net, find("generator"), "insert_individual_query", receipt);
                check(receipt.status = ok);

                for i in 0 to 300 loop
                    walk(tx_clk, 1);
                    check_equal(tx_shift_in, '0');
                end loop;
            elsif run("test_no_reply_for_general_query_if_not_configured") then
                -- Tell the query generator to insert an individual query to 10.10.10.10
                send(net, find("generator"), "insert_general_query", receipt);
                check(receipt.status = ok);

                for i in 0 to 300 loop
                    walk(tx_clk, 1);
                    check_equal(tx_shift_in, '0');
                end loop;
            end if;
        end loop;

        -- Ensure we don't receive anything for several cycles after we're done
        for i in 0 to 100 loop
            walk(tx_clk, 1);
            check_equal(tx_shift_in, '0');
        end loop;

        if not active_python_runner(RUNNER_CFG) then
            get_checker_stat(stat);
            info(LF & "Result:" & LF & to_string(stat));
        end if;

        test_runner_cleanup(runner);
        wait;
    end process;

    -- Generate IGMP queries according to the test
    igmp_query_generation : process
        variable L       : line;
        variable self    : actor_t := create("generator");
        variable message : message_ptr_t;
        variable status  : com_status_t;
        variable receipt : receipt_t;
        -- Inserts a query with the given IP address
        procedure insert_query (
            constant ip : std_logic_vector(31 downto 0)) is
            begin
                info(sformat("Inserting query with IP: %r\n", fo(ip)));

                rx_igmp_multicast_ip <= ip;
                rx_igmp_fifo_empty   <= '0';
                wait until rx_clk = '1';
                while True loop
                    wait until rx_clk = '1';
                    if rx_igmp_shift_out = '1' then
                        exit;
                    end if;
                end loop;
                rx_igmp_multicast_ip <= (others => 'U');
                rx_igmp_fifo_empty   <= '1';
        end procedure;

    begin
        rx_igmp_fifo_empty   <= '1';
        wait until tx_res_n = '1';
        walk(tx_clk, 16);

        while True loop
            receive(net, self, message);
            debug("Received message: " & message.payload.all);
            if message.payload.all = "insert_general_query" then
                insert_query(IGMP_GENERAL_QUERY);
            elsif message.payload.all = "insert_individual_query" then
                insert_query(dot_decimal_to_hex(10, 10, 10, 10));
            else
                assert False
                    report "I don't know what to do with '" & message.payload.all & "'!"
                    severity Failure;
            end if;
        end loop;
        wait;
    end process;

    -- Randomize the TX FIFO full signal
    fifo_full_randomize : process
    begin
        tx_fifo_afull <= '1';
        wait until rx_res_n = '1';
        walk(rx_clk, 10);

        if TX_FIFO_FULL_RANDOMIZE = 0 then
            tx_fifo_afull <= '0';
            wait;
        else
            while True loop
                tx_fifo_afull <= '1';
                walk(rx_clk, random_gen.RandInt(TX_FIFO_FULL_RANDOMIZE));
                tx_fifo_afull <= '0';
                walk(rx_clk, random_gen.RandInt(TX_FIFO_FULL_RANDOMIZE));
            end loop;
        end if;
    end process;

end igmp_reply_tb;

