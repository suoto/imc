---------------
-- Libraries --
---------------
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.std_logic_arith.all;
    use ieee.std_logic_unsigned.all;

package ether_frames_pkg is

    function dot_decimal_to_hex (
        constant a, b, c, d : integer) return std_logic_vector;

    ---------------
    -- Constants --
    ---------------
    -- Generic constants
    constant MAX_PAYLOAD_SIZE   : integer := 1500;
    constant MIN_PAYLOAD_SIZE   : integer := 46;
    -- Ether types
    constant ETHER_TYPE_IPv4    : std_logic_vector(15 downto 0) := x"0800";

    constant MAC_IPv4_MCAST     : std_logic_vector(47 downto 0) := x"01_00_5e_00_00_01";

    -- IGMP query types
    constant IGMP_QUERY         : std_logic_vector(7 downto 0) := x"11";
    constant IGMP_REPORT_v1     : std_logic_vector(7 downto 0) := x"12";
    constant IGMP_REPORT_v2     : std_logic_vector(7 downto 0) := x"16";
    constant IGMP_REPORT_v3     : std_logic_vector(7 downto 0) := x"22";
    constant IGMP_LEAVE         : std_logic_vector(7 downto 0) := x"17";

    -- These use dot_decimal_to_hex, so we declare derefered because they will
    -- be udpated later
    constant IGMP_GENERAL_QUERY     : std_logic_vector;
    constant IGMP_LEAVE_ADDR        : std_logic_vector;
    -- constant IGMP_GROUP_QUERY       : std_logic_vector := dot_decimal_to_hex(224, 0, 0, 1);
    -- constant IGMP_MEMBERSHIP_REPORT : std_logic_vector := dot_decimal_to_hex(224, 0, 0, 1);

    -----------
    -- Types --
    -----------
    type vec8_array_t is array (natural range <>) of std_logic_vector(7 downto 0);
    type vec64_array_t is array (natural range <>) of std_logic_vector(63 downto 0);

    type igmp_frame_t is record
        dst_mac            : std_logic_vector(47 downto 0);
        src_mac            : std_logic_vector(47 downto 0);
        ether_type         : std_logic_vector(15 downto 0);
        ip_version         : std_logic_vector(3 downto 0);
        ip_ihl             : std_logic_vector(3 downto 0);
        ip_total_length    : std_logic_vector(15 downto 0);
        ip_proto           : std_logic_vector(7 downto 0);
        ip_checksum        : std_logic_vector(15 downto 0);
        ip_src             : std_logic_vector(31 downto 0);
        ip_dst             : std_logic_vector(31 downto 0);
        -- IGMP payload
        -- Bits  0    7 8            15 16       31
        --      .------+---------------+----------.
        --    0 | Type | Max Resp Time | Checksum |
        --      +------+---------------+----------+
        --   32 |          Group Address          |
        --      '---------------------------------'
        igmp_type          : std_logic_vector(7 downto 0);
        igmp_max_resp_time : std_logic_vector(7 downto 0);
        igmp_checksum      : std_logic_vector(15 downto 0);
        igmp_address       : std_logic_vector(31 downto 0);
    end record;

end package;

package body ether_frames_pkg is

    -- Converts address from A.B.C.D format to hex(A) & hex(B) & hex(C) & hex(D)
    function dot_decimal_to_hex (
        constant a, b, c, d : integer) return std_logic_vector is
    begin
        return conv_std_logic_vector(a, 8) &
               conv_std_logic_vector(b, 8) &
               conv_std_logic_vector(c, 8) &
               conv_std_logic_vector(d, 8);
    end function;

    constant IGMP_GENERAL_QUERY : std_logic_vector := dot_decimal_to_hex(224, 0, 0, 1);
    constant IGMP_LEAVE_ADDR    : std_logic_vector := dot_decimal_to_hex(224, 0, 0, 2);

end package body;

