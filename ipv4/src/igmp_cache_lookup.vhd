---------------------------------
-- Block name and description --
--------------------------------

---------------
-- Libraries --
---------------
library	ieee;
    use ieee.std_logic_1164.all;
    use ieee.std_logic_arith.all;
    use ieee.std_logic_unsigned.all;

library common_lib;
library memory;
library ipv4;
    use ipv4.ether_frames_pkg.all;

------------------------
-- Entity declaration --
------------------------
entity igmp_cache_lookup is
    generic (
        CACHE_ADDR_WIDTH : integer := 5);
    port (
        -- Usual ports
        clk             : in  std_logic;
        rst             : in std_logic;
        -- IGMP query request interface
        query_en        : in  std_logic;
        query_ip        : in  std_logic_vector(31 downto 0);
        query_busy      : out std_logic;
        -- Interface with the reply frame assembler
        reply_en        : out std_logic;
        reply_ip        : out std_logic_vector(31 downto 0);
        -- IGMP group memory
        igmp_address    : in  std_logic_vector(CACHE_ADDR_WIDTH - 1 downto 0);
        igmp_write_en   : in  std_logic;
        igmp_write_data : in  std_logic_vector(31 downto 0));
end igmp_cache_lookup;

architecture igmp_cache_lookup of igmp_cache_lookup is

    -----------
    -- Types --
    -----------

    -------------
    -- Signals --
    -------------
    signal rd_addr         : std_logic_vector(CACHE_ADDR_WIDTH - 1 downto 0);
    signal rd_addr_delayed : std_logic_vector(CACHE_ADDR_WIDTH - 1 downto 0);
    signal cached_ip       : std_logic_vector(31 downto 0);
    signal busy_i          : std_logic;
    signal rd_dv           : std_logic; -- Read data valid
    signal done_i          : std_logic;

begin

    -------------------
    -- Port mappings --
    -------------------
    -- RAM to store the IGMP groups we are member of
    igmp_groups_u : entity memory.ram_inference
        generic map (
            ADDR_WIDTH         => 5,
            DATA_WIDTH         => 32,
            EXTRA_OUTPUT_DELAY => 1)
        port map (
            -- Port A
            clk_a     => clk,
            clken_a   => '1',
            wren_a    => igmp_write_en,
            addr_a    => igmp_address,
            wrdata_a  => igmp_write_data,
            rddata_a  => open,

            -- Port B
            clk_b     => clk,
            clken_b   => '1',
            addr_b    => rd_addr,
            rddata_b  => cached_ip);

    rd_addr_delay_u : entity common_lib.sr_delay
        generic map (
            DELAY_CYCLES => 2,
            DATA_WIDTH   => 5)
        port map (
            clk   => clk,
            clken => '1',

            din   => rd_addr,
            dout  => rd_addr_delayed);

    busy_delay_u : entity common_lib.sr_delay
        generic map (
            DELAY_CYCLES => 2,
            DATA_WIDTH   => 1)
        port map (
            clk     => clk,
            clken   => '1',

            din(0)  => busy_i,
            dout(0) => rd_dv);

    -----------------------------
    -- Asynchronous assignments --
    -----------------------------
    query_busy <= '1' when busy_i = '1' or rd_dv = '1' else '0';

    ---------------
    -- Processes --
    ---------------
    address_parse_u : process(clk, rst)
    begin
        if rst = '1' then
            rd_addr <= (others => '1');
            busy_i  <= '0';
        elsif clk'event and clk = '1' then
            if query_en = '1' and busy_i = '0' then
                rd_addr <= (others => '0');
                busy_i  <= '1';
            else
                if rd_addr < 2**CACHE_ADDR_WIDTH - 1 then
                    busy_i  <= '1';
                    rd_addr <= rd_addr + 1;
                else
                   busy_i  <= '0';
                end if;
            end if;

        end if;
    end process;

    match_ctrl : process(clk, rst)
    begin
        if rst = '1' then
        elsif clk'event and clk = '1' then
            done_i        <= '0';
            reply_en      <= '0';
            -- If we are able to find the query_ip we're looking for, we must reply to it.
            -- Additionally, if we're handling a general query, all channels must reply
            if rd_dv = '1' then
                reply_ip <= cached_ip;
                if cached_ip /= 0 and 
                    (query_ip = cached_ip or query_ip = IGMP_GENERAL_QUERY) then
                    reply_en <= '1';
                end if;
            end if;
        end if;
    end process;

end igmp_cache_lookup;

