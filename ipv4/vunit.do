configure wave -namecolwidth 287
configure wave -valuecolwidth 100

add wave -noupdate -expand -group "TB top" "*"
add wave -noupdate -expand -group "DUT" "dut/*"

vunit_run
